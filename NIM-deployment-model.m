clear;
clc;
format long;

%------------------------------------------------------------------------------------------
% INTRODUCTION
% This is a tool design to simulate the deployment of a multi element arm affixed to a CubeSat
% The primary purpose of this simulation is to validate the Northern Images Mission Payload of AuroraSat-1.
%------------------------------------------------------------------------------------------


%Assumptions
% 1) Arms are approximated to be solid bars of the same mass and moments of inertia are calculated at the one extreme of thier length.

%Outputs
% 1) The temporal evolution of the angle of all three arms.
% 2) The speed at which the arms reach their limit stops.

%------------------------------------------------------------------------------------------
%Analysis Parameters
%------------------------------------------------------------------------------------------


%------------------------------------------------------------------------------------------
%Geometric Parameters
%------------------------------------------------------------------------------------------

% -- Inner Arm Denoted with leading I
% -- -- Properties
I.length = 0.070; % [m]
I.mass = 0.00769; % [kg]
I.moment = (1/3) * I.mass * (I.length)^2 % [kgm^2]
% -- -- Position
I.a.x = 0; % fixed
I.a.y = 0; % fixed
I.b.x = 0; % driven
I.b.y = 0; % driven
I.rot = 0; %

% -- Outer Arm Properties: Denoted with Leading O
O.length = 0.070; % [m]
O.mass = 0.00769; % [kg]
O.moment = (1/3) * I.mass * (I.length)^2 % [kgm^2]
% -- -- Position
O.a.x = I.b.x; % Affixed to Inner Arm
O.a.y = I.b.y; % Affixed to Inner Arm
O.b.x = 0; % driven
O.b.y = 0; % driven
O.rot = 0; %

% -- Screen Properties: Denoted with Leading S
S.length = 0.070; % [m]
S.mass = 0.00769; % [kg]
S.moment = (1/3) * I.mass * (I.length)^2 % [kgm^2]
% -- -- Position
S.a.x = O.b.x; % Affixed to Outer Arm
S.a.y = O.b.y; % Affixed to Outer Arm
S.b.x = 0; % driven
S.b.y = 0; % driven
S.rot = 0; %

% -- Spring Rate Definitions


%------------------------------------------------------------------------------------------
%Step Through Analysis
%------------------------------------------------------------------------------------------

%------------------------------------------------------------------------------------------
%Outputs
%------------------------------------------------------------------------------------------
