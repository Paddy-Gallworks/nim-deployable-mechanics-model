import math

# SETUP

## Point Nomenclature

#         /
#        /
#       C   Outer Pivot
#        \
#         \
#          \
#           B  Intermediate Pivot
#          /
#         /
#        /
#-------A-- Payload Main Board

# MECHANICAL PROPERTIES

## Inner Arm Physical Properties
a_one_mass = 0.008 #kilograms
a_one_length = 0.070 #m
a_one_I = (1/3) * a_one_mass * math.pow(a_one_length,2)# calculated around Point A

## Outer Arm Elements
a_two_mass = 0.0045 #kilograms
a_two_length = 0.058 #m
a_two_I = (1/3) * a_two_mass * math.pow(a_two_length,2)# calculated around Point B

## Screen Elements
a_three_mass = 0.008 # kilograms
a_three_length = 0.045 #m
a_three_I = (1/3) * a_three_mass * math.pow(a_three_length,2)# calculated around Point B

# Spring Rates (Units are Nm / Rad)
k_one = 0.0001 # Torsion Spring Around Inner Pivot - Previous value 0.028
k_two = 0.0001 # Torsion Spring at Intermediate Pivot
k_three = 0.001 # Torsion Spring at Outer Pivot

## Display Summary of Arm Properties

# SIMULATION CONFIGURATION

time_step = 1 # milliseconds
simulation_duration = 100 # milliseconds

# SIMULATION VARIABLES

## Arm Position Variables
# All angle units are RADIANS
theta_one = 0 # the effective angle may change with the development of the collision-less arm models
theta_two = 0 # measured between arm element one and two
theta_three = 0 # measured between arm element two and three
theta_int = 180 # absolute position of arm B, calculated from theta_one and theta_two
theta_ext = 0 # absolute position of arm C, calculated from theta_two and thetha_three

### Angular Velocities
theta_one_dot = 0
theta_two_dot = 0
theta_three_dot = 0
theta_int_dot = 0

### Angular Accelerations
theta_one_dot_dot = 0
theta_two_dot_dot = 0
theta_three_dot_dot = 0
theta_int_dot_dot = 0

### Linear Accelerations
a_b_x = 0
a_b_y = 0
a_c_x = 0
a_c_y = 0

# STEP THROUGH ANALYSIS
#print("t(ms)\tTh1\tTh2\tTh3\tTh1d\tTh1dd\tTh2d")
for time in range(0,simulation_duration,time_step):
    #absolute position of arms
    theta_int = (3.14 - theta_two) + theta_one 
    theta_ext = theta_three + theta_int - 3.14 # Page 6 Eq 1
    ## Calculate Instantaneous Forces
    ### Spring Torques (assume constant?)
    #t_three = k_three
    #t_two = k_two
    #t_one = k_one
    
    t_three = k_three * (3.14 - theta_three) # originally 2.09 was used here
    t_two = k_two * (3.14 - theta_two)
    t_one = k_one * (3.14 - theta_one)
    ### Forces and Torques
    f_c_x = a_three_mass * a_c_x # Page 7 EQ 5
    f_c_y = a_three_mass * a_c_y # Page 7 EQ 6
    f_b_x = f_c_x - (a_two_mass * a_b_x) # Page 7 EQ 2
    f_b_y = f_c_y - (a_two_mass * a_b_y) # Page 7 EQ 3
    t_c_sum = t_three # Page 7 EQ 1 LHS
    t_b_sum = - t_two - t_three - (f_c_x * a_two_length * math.cos(theta_int - (3.14/2))) - (f_c_y * a_two_length * math.sin(theta_int- (3.14/2))) # see page 7 : EQ 4 left hand side
    t_a_sum = t_one + t_two + (f_b_y * a_one_length * math.cos(theta_one)) - (f_b_x * a_one_length * math.sin(theta_one)) # Page 7 EQ 9 left hand side
    ## Calculate Instantaneous Accelerations
        ## These calculations should be based on previous angular accelerations, not based on forces!
    a_b_x = - a_one_length * theta_one_dot_dot * math.sin(theta_one) 
    a_b_y = a_one_length * theta_one_dot_dot * math.cos(theta_one) # See Page 1
    a_c_x = a_b_x + (theta_two_dot_dot * a_two_length * math.cos((3.14)-theta_int)) # Page 7 Eq 7
    a_c_y = a_b_y + (theta_two_dot_dot * a_two_length * math.sin((3.14)-theta_int)) # Page 7 Eq 8
    ## Calculate Instantaneous Accelerations
    ### Arm One
    theta_one_dot_dot = t_a_sum / a_one_I
    ### Arm Two
    theta_int_dot_dot = t_b_sum / a_two_I
    theta_two_dot_dot = - theta_int_dot_dot # Note: the angles 'grow' in different directions!
    ### Arm Three
    theta_three_dot_dot = t_c_sum / a_three_I # EQ_1
    ## Apply Velocities Over Time Step
    ### Arm One
    theta_one_dot = theta_one_dot + (theta_one_dot_dot * (time_step/1000))
    theta_one = theta_one + theta_one_dot * (time_step/1000)
    if (theta_one > 1.57):
        # set theta one to max possible value:
        theta_one = 1.57
        #theta_one_dot = 0
        theta_one_dot_dot = 0
    if (theta_one < 0):
        theta_one = 0
        theta_one_dot = 0
        theta_one_dot_dot = 0
    ### Arm Two
    theta_two_dot = theta_two_dot + (theta_two_dot_dot * (time_step/1000))
    theta_two = theta_two + (theta_two_dot * (time_step/1000))
    if (theta_two < 0): # Min Angle between arms
        theta_two = 0
        theta_two_dot = 0
        theta_two_dot_dot = 0
    if (theta_two > 3.14): # Max Angle of Theta_two
        theta_two = 3.14
        #theta_two_dot = 0
        theta_two_dot_dot = 0
    ### Arm Three
    theta_three_dot = theta_three_dot + (theta_three_dot_dot * (time_step/1000))
    theta_three = theta_three + (theta_three_dot * (time_step/1000))
    if (theta_three < 0): # Min Angle between arms
        theta_three = 0
        theta_three_dot = 0
        theta_three_dot_dot = 0
    if (theta_three > 1.7): # Max Angle of theta_three
        theta_three = 1.7
        theta_three_dot = 0
        theta_three_dot_dot = 0
    ### Print Position -> The format is to match an excel document!
    print('{0:3d}, {1:.4f}, {2:6.4f}, {3:6.4f}, {4:6.4f}, {5:6.4f}, {6:6.4f}, {7:6.4f}, {8:6.4f}, {9:6.4f},{10:.4f},{11:.4f},{12:.4f},{13:.4f},{14:.4f},{15:.4f},{16:.4f}'.format(time, theta_one, theta_one_dot, theta_two, theta_two_dot, theta_three, theta_three_dot, theta_one_dot_dot, theta_two_dot_dot,theta_three_dot_dot,t_one,t_two,t_three,f_b_x,f_b_y,f_c_x,f_c_y))
print("Simulation Complete")

