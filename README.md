# NIM Deployable Mechanics Model

This project was created to support the development of the Aurora College CubeSat.

# Commit Log:

### Single Arm Test

34bf703f9b116120371226d0444a7727a78cc919

- The model is assuming arm two is a lump mass at the end of arm one.
- Only arm one is being resolved force wise.
- Only the inner spring torque is being modelled.